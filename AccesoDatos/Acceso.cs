﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace AccesoDatos
{
    public class Acceso
    {
        SqlConnection conexion;
        public void Abrir()
        {
            conexion = new SqlConnection();
            conexion.ConnectionString = @"Data Source= .\SQLEXPRESS; Initial Catalog= Genera; Integrated Security =  SSPI";
            conexion.Open();
        }

        public void Cerrar()
        {
            conexion.Close();
            conexion.Dispose();
            conexion = null;
            GC.Collect();
        }
    }
}
