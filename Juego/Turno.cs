﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Juego
{
    public class Turno
    {
		private int numero;

		public int Numero
		{
			get { return numero; }
			set { numero = value; }
		}

		private int cantiTiros = 3;

		public int CantiTiros
		{
			get { return cantiTiros; }
		}

	}
}
