﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoDatos;
using System.Data.SqlClient;
using System.Data;

namespace Juego
{
    public class Jugador
    {

        Acceso acceso = new Acceso();

        private int id;

        public int ID
        {
            get { return id; }
            set { id = value; }
        }

        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        private Credencial credencial = new Credencial();

        public Credencial Credencial
        {
            get { return credencial; }
            set { credencial = value; }
        }

        private int puntaje;

        public int Puntaje
        {
            get { return puntaje; }
        }

        private int partidasGanadas;

        public int PartidasGanadas
        {
            get { return partidasGanadas; }
        }

        private int partidasEmpatadas;

        public int PartidasEmpatadas
        {
            get { return partidasEmpatadas; }
        }

        private int partidasPerdidas;

        public int PartidasPerdidas
        {
            get { return partidasPerdidas; }
        }


        private Turno turno = new Turno();
        public Turno Turno
        {
            get { return turno; }
            set { turno = value; }
        }

        private Dado[] dados = new Dado[5];
        public Dado[] Dados
        {
            get { return dados; }
        }
    }
}
