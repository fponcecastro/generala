﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Juego
{
    public class Generala
    {
        public static bool Verificar(Jugador j)
        {
            bool ok = false;
            if (j.Dados[0].Valor == j.Dados[1].Valor && 
                j.Dados[1].Valor == j.Dados[2].Valor && 
                j.Dados[2].Valor == j.Dados[3].Valor &&
                j.Dados[3].Valor == j.Dados[4].Valor &&
                j.Dados[4].Valor == j.Dados[5].Valor &&
                j.Dados[5].Valor == j.Dados[6].Valor)
            {
                ok = true;
            }
            return ok;
        }
    }
}
