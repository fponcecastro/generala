﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Juego
{
    public class Dado
    {
        static Random rnd;
        static Dado()
        {

            DateTime ahora = DateTime.Now;
            ahora.AddTicks(DateTime.Now.Ticks);
            int semilla = (int)ahora.Millisecond;
            rnd = new Random(semilla);
        }

        private int valor;

        public int Valor
        {
            get { return valor; }
            set { valor = value; }
        }

        private bool retenido = false;

        public bool Retenido
        {
            get { return retenido; }
            set { retenido = value; }
        }
        public void Tirar()
        {
            if (!retenido)
            {
                valor = rnd.Next(1, 7);
            }

        }
    }
}
